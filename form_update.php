<?php
require "begin.html";
?>
<?php
require_once "Model.php";
$model = new Model();
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $nobel = $model->get_nobel_by_id($id);
    if ($nobel) {
?>
        <form action='update.php' method='post'>
            <label for='id'>ID </label>
            <input name='id' value='<?php echo $nobel['id']; ?>' /><br />
            <label for='name'>Name</label>
            <input type='text' name='name' id='name' value='<?php echo $nobel['name']; ?>' /><br />
            <label for='birthdate'>Birthdate</label>
            <input type='text' name='birthdate' id='birthdate' value='<?php echo $nobel['birthdate']; ?>' /><br />
            <label for='birthplace'>Birthplace</label>
            <input type='text' name='birthplace' id='birthplace' value='<?php echo $nobel['birthplace']; ?>' /><br />
            <label for='country'>Country</label>
            <input type='text' name='country' id='country' value='<?php echo $nobel['county']; ?>' /><br />
            <label for='category'>Category</label>
            <?php
            $categories = $model->get_categories();
            foreach ($categories as $category) {
                if ($category == $nobel['category']) {
            ?>
                    <input type='radio' name='category' value='<?php echo $category; ?>' checked><?php echo $category; ?><br>
                <?php
                } else {
                ?>
                    <input type='radio' name='category' value='<?php echo $category; ?>'><?php echo $category; ?><br>
            <?php
                }
            }
            ?>
            <label for='year'>Year</label>
            <input type='text' name='year' id='year' value='<?php echo $nobel['year']; ?>' /><br />
            <label for='motivation'>Motivation</label>
            <textarea name='motivation' cols=50 rows=5 id='motivation'><?php echo $nobel['motivation']; ?></textarea><br />
            <input type='submit' name='submit' value='Update' />
        </form>
<?php
    } else {
        echo "There is no nobel prize with such id.";
    }
}
?>