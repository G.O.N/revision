<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo "Page" . $_GET['page']; ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <style>
        .pagination {
            display: flex;
            justify-content: center;
        }

        .pagination a {
            text-decoration: none;
            color: #333;
            border: 1px solid #ddd;
            margin: 0 4px;
        }



        .pagination a:first-of-type {
            margin-left: 0;
            font-size: 2rem;
        }

        .pagination a:last-of-type {
            margin-right: 0;
            font-size: 2rem;

        }

        .pagination a:first-of-type,
        .pagination a:last-of-type {
            background-color: #f1f1f1;
            color: black;
            font-weight: bold;
        }
    </style>

</head>

<body>
    <?php
    require "begin.html";
    ?>

    <div class="container-fluid my-2">
        <?php
        require_once "Model.php";
        $model = new Model();

        $total_prizes = $model->get_nb_nobel_prizes();
        $prizes_per_page = 25;
        $total_pages = ceil($total_prizes / $prizes_per_page);

        $page = 1;
        if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $page = (int)$_GET['page'];
        }
        if ($page < 1) {
            $page = 1;
        }
        if ($page > $total_pages) {
            $page = $total_pages;
        }

        $offset = ($page - 1) * $prizes_per_page;
        $nobels = $model->get_nobel_prizes_with_limit($offset, $prizes_per_page);

        echo "<table class='table border table-striped'>";
        echo "<tr>
                <th>Id</th>
                <th>Name</th>
                <th>Birthdate</th>
                <th>Birthplace</th>
                <th>Year</th>
                <th>Country</th>
                <th>Category</th>
                <th>Motivation</th>
                
            </tr>";
        foreach ($nobels as $nobel) {
            echo "<tr>";
            echo "<td>{$nobel['id']}</td>";
            echo "<td>{$nobel['name']}</td>";
            echo "<td>{$nobel['birthdate']}</td>";
            echo "<td>{$nobel['birthplace']}</td>";
            echo "<td>{$nobel['year']}</td>";
            echo "<td>{$nobel['county']}</td>";
            echo "<td>{$nobel['category']}</td>";
            echo "<td>{$nobel['motivation']}</td>";
            echo "</tr>";
        }
        echo "</table>";

        $pagination_start = max(1, $page - 12);
        $pagination_end = min($total_pages, $pagination_start + $prizes_per_page - 1);

        $pagination_html = "<div class='pagination'>";
        if ($page > 1) {
            $prev_page = $page - 1;
            $pagination_html .= "<a href='?page=$prev_page'>&#8249;</a>";
        }
        for ($i = $pagination_start; $i <= $pagination_end; $i++) {
            if ($i === $page) {
                $pagination_html .= "<strong>$i</strong>";
            } else {
                $pagination_html .= "<a href='?page=$i'>$i</a>";
            }
        }
        if ($page < $total_pages) {
            $next_page = $page + 1;
            $pagination_html .= "<a href='?page=$next_page'>&#8250;</a>";
        }
        $pagination_html .= "</div>";


        echo $pagination_html;

        ?>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
</body>

</html>