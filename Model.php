<?php
class Model
{
    public $connexion;

    public function __construct()
    {
        try {
            if ($this->connexion = new PDO(
                'mysql:host=localhost;dbname=noble;charset=utf8',
                'root',
                ''
            )) {
                // echo " success to connetion";
            } else {
                throw new Exception('Unable to connect');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function get_last()
    {
        $req = $this->connexion->query('SELECT * FROM nobels ORDER BY year DESC LIMIT 25');
        return $req;
    }
    public function get_nb_nobel_prizes()
    {
        $req = $this->connexion->query('SELECT COUNT(*) FROM nobels');
        $z = $req->fetch();
        return $z[0];
    }

    public function get_nobel_by_id($id)
    {
        $sql = "SELECT * FROM nobels WHERE id = :id";
        $re = $this->connexion->prepare($sql);
        $re->execute(array(':id' => $id));
        if ($re->rowCount() == 1) {
            return $re->fetch(PDO::FETCH_ASSOC);
        } else {
            return false;
        }
    }

    public function get_nobel_prize_informations($id)
    {
        $requete = $this->connexion->query("Select * from nobels WHERE id =$id");
        return $requete->fetch();
    }
    public function get_categories()
    {
        $requete = $this->connexion->query('SELECT * FROM categories');
        $categorie = [];
        foreach ($requete as $re) {
            $categorie[] = $re[0];
        }
        return $categorie;
    }
    public function add_nobel_prize()
    {
        $year =  $_REQUEST['year'];
        $categorie = $_POST['category'];
        $name = $_POST['name'];
        $bd = $_POST['bd'];
        $bp = $_POST['bp'];
        $country = $_POST['country'];
        $motivation = $_POST['motivation'];
        $requete = $this->connexion->query("INSERT INTO nobels(year, category, name, birthdate, birthplace, county, motivation) 
        VALUES('$year','$categorie','$name','$bd', '$bp', '$country','$motivation')");

        return $requete;
    }
    public function remove_nobel_prize($id)
    {
        $requete = $this->connexion->query("delete from nobels WHERE id =$id");
        return $requete->fetch();
    }
    private static $instance = null;

    public static function getModel()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public function update_Nobel_Prize($data)
    {
        $sql = "UPDATE nobels SET year = :year, category = :category, name = :name, birthdate = :birthdate, birthplace = :birthplace, county = :county, motivation = :motivation WHERE id = :id";
        $stmt = $this->connexion->prepare($sql);
        $stmt->execute(array(
            ':year' => $data['year'],
            ':category' => $data['category'],
            ':name' => $data['name'],
            ':birthdate' => $data['birthdate'],
            ':birthplace' => $data['birthplace'],
            ':county' => $data['county'],
            ':motivation' => $data['motivation'],
            ':id' => $data['id']
        ));
    }
    public function get_nobel_prizes_with_limit($offset, $limit)
    {
        $sql = "Select * from nobels ORDER BY year DESC LIMIT $limit OFFSET $offset";
        $stmt = $this->connexion->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
