<?php
require "begin.html";
?>
<h1> Updating a nobel prize </h1>
<?php
function check_data()
{
    if (
        isset($_POST["name"]) && trim($_POST["name"]) !== "" &&
        isset($_POST["category"]) && trim($_POST["category"]) !== "" &&
        isset($_POST["year"]) && $_POST["year"] > 0
    ) {
        if (!isset($_POST["name"]) or trim($_POST["name"]) === "") {
            $_POST["name"] = null;
        }
        if (!isset($_POST["year"]) or trim($_POST["year"]) === "") {
            $_POST["year"] = null;
        }
        if (!isset($_POST["country"]) or trim($_POST["country"]) === "") {
            $_POST["country"] = null;
        }
        if (!isset($_POST["category"]) or trim($_POST["category"]) === "") {
            $_POST["category"] = null;
        }
        if (!isset($_POST["bd"]) or trim($_POST["bd"]) === "") {
            $_POST["bd"] = null;
        }
        if (!isset($_POST["bp"]) or trim($_POST["bp"]) === "") {
            $_POST["bp"] = null;
        }
        if (!isset($_POST["motivation"]) or trim($_POST["motivation"]) === "") {
            $_POST["motivation"] = null;
        }
        if (
            isset($_POST["bp"])  && trim($_POST["bp"]) !== "" && trim($_POST["year"]) !== "" && trim($_POST["name"]) !== ""
            && trim($_POST["category"]) !== "" && trim($_POST["country"]) !== "" && trim($_POST["bd"]) !== "" && trim($_POST["motivation"]) !== ""
        )

            return false;
    }
}
require_once "Model.php";

if (!check_data()) {
    $info = array(
        "id" => $_POST['id'],
        "name" => $_POST['name'],
        "birthdate" => $_POST['birthdate'],
        "birthplace" => $_POST['birthplace'],
        "year" => $_POST['year'],
        "county" => $_POST['country'],
        "category" => $_POST['category'],
        "motivation" => $_POST['motivation']
    );

    $e = new Model;
    $ex = $e->update_Nobel_Prize($info);
    echo "Noble Updated with successfuly";
} else {
    echo " nothing to Update ";
}
